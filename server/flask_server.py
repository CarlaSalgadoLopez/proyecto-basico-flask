from flask_restplus import Api, Resource, fields
from database.mongodb import Mongodb
from flask import Flask, request, jsonify, send_file
from werkzeug.datastructures import FileStorage

import os

app = Flask(__name__)
mongodb = Mongodb()

api = Api(app, version='1.0', title='Mi primer gestor de jugadores',
          description='API REST para la gestión de los jugadores de mi aplicación web')

ns_jugadores = api.namespace('mi-app/v1/jugadores')

json_success = {200: 'Success'}
json_created = {201: 'Created'}
json_deleted = {204: 'Deleted'}
json_updated = {204: 'Updated'}
json_dontfound = {404: 'Resource dont found'}
json_exist = {409: "Already exist"}

model_get_jugador = api.model('Jugador', {
    "_id": fields.String(required=True,
                         example="5c74642fd44c3144743107b7", description="Identificador del jugador"),
    "name": fields.String(required=True, example="Gamercat", description="Nombre del jugador"),
    "username": fields.String(required=True, example="TheBestCatEver", description="Nombre de usuario unico"),
    "age": fields.Integer(required=True, example=7, description="Edad del jugador"),
    "games": fields.List(fields.String, required=True,
                         example=["Pokemon", "Assassin's creed"], description="Lista de juegos del jugador"),
    "photo": fields.String(example="/home/user/Gamers/Gamercat.jpg", description="Foto de perfil del jugador")
})

model_put_jugador = api.model('Jugador', {
    "name": fields.String(required=True, example="Gamercat", description="Nombre del jugador"),
    "username": fields.String(required=True, example="TheBestCatEver", description="Nombre de usuario unico"),
    "age": fields.Integer(required=True, example=7, description="Edad del jugador"),
    "games": fields.List(fields.String, required=True,
                         example=["Pokemon", "Assassin's creed"], description="Lista de juegos del jugador"),
    "photo": fields.String(example="/home/user/Gamers/Gamercat.jpg", description="Foto de perfil del jugador")
})


@ns_jugadores.route('/')
class Jugadores(Resource):
    parser_get_jugadores = api.parser()
    parser_get_jugadores.add_argument('edad', location='args', help='Edad del jugador')
    parser_get_jugadores.add_argument('offset', location='args', help='Numero de jugadores que nos saltamos')
    parser_get_jugadores.add_argument('limit', location='args', help='Numero de jugadores por página')
    parser_post_jugadores = api.parser()
    parser_post_jugadores.add_argument('jugador', type=list, location='json', help='datos del jugador', required=True)

    @api.marshal_with(model_get_jugador, as_list=True)
    @api.expect(parser_get_jugadores)
    @api.doc(responses={200: 'Success'})
    def get(self):
        """ Devuelve todos los jugadores filtrados o no por edad con paginación """
        arguments = request.args
        # Equivalente a sin limite
        offset = 0
        limit = 0
        edad = {}
        if 'edad' in arguments:
            edad = {'age': {"$gt": int(arguments['edad'])}}

        if 'offset' in arguments:
            offset = int(arguments['offset'])
        if 'limit' in arguments:
            limit = int(arguments['limit'])

        return mongodb.find_by_key_value(edad, offset, limit), 200

    @api.expect(model_put_jugador)
    @api.doc(responses={409: 'Already exist', 201: 'Created'})
    def post(self):
        """ Añade un jugador a la base de datos"""
        content = request.get_json(force=True)
        result = mongodb.create(content)
        if result == -1:
            response = jsonify(json_exist)
            response.status_code = 409
            return response
        else:
            response = jsonify(result)
            response.status_code = 200
            return response


@ns_jugadores.route('/<jugador_id>')
class Jugador(Resource):
    parser_put_jugador = api.parser()
    parser_put_jugador.add_argument('Jugador', type=list, location='json', help='Datos del jugador', required=True)

    @api.marshal_with(model_get_jugador)
    @api.doc(responses={200: 'Success', 404: 'Resource dont found'})
    def get(self, jugador_id):
        """ Busca un jugador por id en la basee de datos """

        element = mongodb.find(jugador_id)
        if element is None:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response
        else:
            return element, 200

    @api.expect(parser_put_jugador)
    @api.doc(responses={204: 'Success', 404: 'Resource dont found'})
    def put(self, jugador_id):
        """ Aactualiza un jugador de la base de datos """

        content = request.get_json(force=True)
        result = mongodb.update(jugador_id, content)
        if result == -1:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response
        else:
            response = jsonify(json_updated)
            response.status_code = 204
            return response

    @api.doc(responses={204: 'Success', 404: 'Resource dont found'})
    def delete(self, jugador_id):
        """ Elimina un jugador de la base de datos """

        element = mongodb.delete(jugador_id)
        if element == -1:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response
        else:
            response = jsonify(json_deleted)
            response.status_code = 204
            return response


@ns_jugadores.route('/<jugador_id>/image')
class GetImageGamer(Resource):

    @api.doc(responses={200: 'Sucess', 404: 'Resource dont found'})
    def get(self, jugador_id):
        """ Obtiene la imagen del jugador"""

        jugador = mongodb.find(jugador_id)
        if jugador is None:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response

        try:
            path = jugador['photo']
            mime = 'image/jpg'
            return send_file(path, mime)
        except FileNotFoundError:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response


@ns_jugadores.route('/files/<filename>')
class PutImagen(Resource):
    upload_parser = api.parser()
    upload_parser.add_argument('image', type=FileStorage, location='files',
                               help='Imagen del jugador', required=True)

    UPLOAD_FOLDER = 'static/img'

    @api.expect(upload_parser)
    @api.doc(responses={200: 'Success', 404: 'Resource dont found'})
    def post(self, filename):
        """ Guarda una imagen en el servidor """

        if 'image' in request.files:
            file = request.files['image']
            extension = file.filename.split('.')[1]
            path = "C:/Users/oysho/Documents/Gamers"
            if not os.path.exists(path):
                os.makedirs(path)
            path_file = path + filename + '.' + extension
            file.save(path_file)
            response = jsonify(json_success)
            response.status_code = 200
            return response
        else:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response


@ns_jugadores.route('/<jugador_id>/juegos')
class Juegos(Resource):
    parser_put = api.parser()
    parser_put.add_argument('juegos', location='form', action='split',
                            help='Lista con los nuevos juegos del jugador', required=True)

    parser_post = api.parser()
    parser_post.add_argument('juego', location='form', help='Nuevo juego del jugador', required=True)

    @api.expect(parser_put)
    @api.doc(responses={204: 'Success', 404: 'Resource dont found'})
    def put(self, jugador_id):
        """ Reemplaza la lista de juegos del jugador por otra nueva """

        args = self.parser_put.parse_args()
        juegos = args['juegos']
        jugador = mongodb.find(jugador_id)
        if jugador is None:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response
        else:
            jugador['games'] = juegos
            mongodb.update(jugador_id, jugador)
            response = jsonify(json_updated)
            response.status_code = 204
            return response

    @api.expect(parser_post)
    @api.doc(responses={204: 'Success', 404: 'Resource dont found'})
    def post(self, jugador_id):
        """ Añade un juego a la lista de juegos del jugador """

        args = self.parser_post.parse_args()
        juego = args['juego']
        jugador = mongodb.find(jugador_id)
        if jugador is None:
            response = jsonify(json_dontfound)
            response.status_code = 404
            return response
        else:
            juegos = jugador['games']
            juegos.append(juego)
            jugador['games'] = juegos
            mongodb.update(jugador_id, jugador)
            response = jsonify(json_updated)
            response.status_code = 204
            return response


if __name__ == '__main__':
    app.run()
