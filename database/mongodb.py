from pymongo.errors import DuplicateKeyError
from pymongo import MongoClient
import bson


class Mongodb:
    mongo_client = MongoClient('mongodb://localhost:27017/')
    __db = mongo_client['MyDatabase']
    dictionary = __db['Gamer']

    def create(self, data):
        self.dictionary.create_index('username', unique=True)
        try:
            inserted_id = self.dictionary.insert_one(data).inserted_id
            element = self.dictionary.find_one({'_id': bson.ObjectId(inserted_id)})
            if element is not None:
                element['_id'] = str(element['_id'])
            return element

        except DuplicateKeyError:
            return -1

    def find(self, primary_key):
        data = self.dictionary.find_one({'_id': bson.ObjectId(primary_key)})
        if data is not None:
            data['_id'] = str(data['_id'])
        return data

    def find_by_key_value(self, filter, count, page_size):
        elements = []
        datas = list(self.dictionary.find(filter).skip(count).limit(page_size))
        if len(datas) > 0:
            for data in datas:
                data['_id'] = str(data['_id'])
                elements.append(data)
            return datas
        else:
            return []

    def update(self, primary_key, data):
        if '_id' in data:
            del data['_id']
        self.dictionary.replace_one({'_id': bson.ObjectId(primary_key)}, data)

    def delete(self, primary_key):
        self.dictionary.delete_one({'_id': bson.ObjectId(primary_key)})

    def get_indexes(self):
        return self.dictionary.list_indexes()

